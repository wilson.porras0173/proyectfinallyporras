<h1 class="text-center"><i class=""></i> ACTUALIZAR DATOS</h1>
<form class=""
id="frm_actualizar_detalle"
action="<?php echo site_url('detalles/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
          <div class="col-md-4">
            <input type="hidden" name="id_det" id="id_det" value="<?php echo $detalleEditar->id_det; ?>">
              <label for="">FK_ID_LEC:
                <span class="obligatorio">(Required)</span>
              </label>
              <br>
              <input type="text"
              placeholder="Ingrese el id de lectura"
              class="form-control"
              required
              name="fk_id_lec" value="<?php echo $detalleEditar->fk_id_lec; ?>"
              id="fk_id_lec">
              </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_rec">FK_ID_REC:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el id de recaudacion" class="form-control" required name="fk_id_rec"  value="<?php echo $detalleEditar->fk_id_rec; ?>" id="fk_id_rec">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="cantidad_det">CANTIDAD:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la cantidad" class="form-control" required name="cantidad_det" value="<?php echo $detalleEditar->cantidad_det; ?>" id="cantidad_det">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="valor_unitario_det">VALOR UNITARIO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el valor unitario" class="form-control" required name="valor_unitario_det" value="<?php echo $detalleEditar->valor_unitario_det; ?>" id="valor_unitario_det">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="subtotal_det">SUBTOTAL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el subtotal" class="form-control" required name="subtotal_det" value="<?php echo $detalleEditar->subtotal_det; ?>" id="subtotal_det">
            </div>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="iva_det">IVA:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el iva" class="form-control" required name="iva_det" value="<?php echo $detalleEditar->iva_det; ?>" id="iva_det">
            </div>
        </div>
    </div>
</center>
    <br>
    <div class="row">
      <div class="col-md-1">
      </div>
      <div class="col-md-9">
        <div class="form-group">
            <label for="detalle_det">DETALLE:
                <span class="obligatorio">(Required)</span>
            </label>
            <input type="text" placeholder="Ingrese un detalle" class="form-control" required name="detalle_det" value="<?php echo $detalleEditar->detalle_det; ?>" id="detalle_det">
        </div>
      </div>
      <div class="col-md-1">
      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-success">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/detalles/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_detalle").validate({
   rules:{
     fk_id_lec:{
         required:true,
     },
     fk_id_rec:{
         required:true,
     },
     cantidad_det:{
       required:true,
     },
     valor_unitario_det:{
       required:true,
     },
     subtotal_det:{
       required:true,
     },
     iva_det:{
       required:true,
     },
     detalle_det:{
       required:true,
     }
   },
   messages:{
     fk_id_lec:{
         required:"Porfavor, ingrese el id de lectura",
     },
     fk_id_rec:{
         required:"Porfavor, ingrese el id de recaudacion",
     },
     cantidad_det:{
       required:"Porfavor, ingrese la cantidad",
     },
     valor_unitario_det:{
       required:"Porfavor, ingrese el valor unitario",
     },
     subtotal_det:{
       required:"Porfavor, ingrese el subtotal",
     },
     iva_det:{
       required:"Porfavor, ingreso el iva",
     },
     detalle_det:{
       required:"Porfavor, ingreso un detalle",
     }
   }
 });

 </script>
