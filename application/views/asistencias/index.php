<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalAsistencias = 0;
$mostSeatsStadium = null;

if ($listadoAsistencias) {
    $totalAsistencias = sizeof($listadoAsistencias);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoAsistencias as $asistenciaTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE ASISTENCIAS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('asistencias/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Asistencia</a>
</center>
<br>
</div>
<br>
<?php if ($listadoAsistencias): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_asistencias">
      <thead>
        <tr>
          <th style="color:white;">ID</th>
          <th style="color:white;">FK_ID_EVE</th>
          <th style="color:white;">FK_ID_SOC</th>
          <th style="color:white;">TIPO</th>
          <th style="color:white;">VALOR</th>
          <th style="color:white;">ATRASO</th>
          <th style="color:white;">VALOR ATRASO</th>
          <th style="color:white;">CREACION</th>
          <th style="color:white;">ACTUALIZACION</th>
          <th style="color:white;">ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoAsistencias as $asistenciaTemporal): ?>
          <tr>
            <td style="color:white;"><?php echo $asistenciaTemporal->id_asi ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->fk_id_eve ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->fk_id_soc ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->tipo_asi ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->valor_asi ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->atraso_asi ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->valor_atraso_asi ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->creacion_asi ?></td>
            <td style="color:white;"><?php echo $asistenciaTemporal->actualizacion_asi ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/asistencias/actualizar/<?php echo $asistenciaTemporal->id_asi; ?>" title="Editar Asistencia">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/asistencias/borrar/<?php echo $asistenciaTemporal->id_asi; ?>" title="Eliminar perfil" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalAsistencias; ?>
        </h5>
        <p class="card-text">Registro de Asistencias</p>
      </div>
    </div>
  </div>
</div>
<br>
<script type="text/javascript">
  $("#tbl_asistencias").DataTable();
</script>
