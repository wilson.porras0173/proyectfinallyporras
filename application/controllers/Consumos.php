<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consumos extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("consumo");
	}
	//renderiza la vista index de consumo
	public function index()
	{
		$data["listadoConsumos"]=
		$this->consumo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('consumos/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de consumo
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('consumos/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarConsumo(){
		$datosNuevoConsumo=array(
			"anio_consumo"=>$this->input->post('anio_consumo'),
			"mes_consumo"=>$this->input->post('mes_consumo'),
      "estado_consumo"=>$this->input->post('estado_consumo'),
			"fecha_creacion_consumo"=>$this->input->post('fecha_creacion_consumo'),
      "fecha_actualizacion_consumo"=>$this->input->post('fecha_actualizacion_consumo'),
      "numero_mes_consumo"=>$this->input->post('numero_mes_consumo'),
      "fecha_vencimiento_consumo"=>$this->input->post('fecha_vencimiento_consumo')
		);
    if($this->consumo->insertar($datosNuevoConsumo)){
				$this->session
				->set_flashdata('confirmacion',
			 'Consumo insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('consumos/index');
	}
	//funcion para eliminar consumo
	public function borrar($id_consumo){
		if ($this->consumo->eliminarPorId($id_consumo)){
			$this->session
			->set_flashdata('confirmacion',
		 'Consumo ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('consumos/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["consumoEditar"]=
			$this->consumo->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("consumos/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosConsumoEditado=array(
      "anio_consumo"=>$this->input->post('anio_consumo'),
			"mes_consumo"=>$this->input->post('mes_consumo'),
      "estado_consumo"=>$this->input->post('estado_consumo'),
			"fecha_creacion_consumo"=>$this->input->post('fecha_creacion_consumo'),
      "fecha_actualizacion_consumo"=>$this->input->post('fecha_actualizacion_consumo'),
      "numero_mes_consumo"=>$this->input->post('numero_mes_consumo'),
      "fecha_vencimiento_consumo"=>$this->input->post('fecha_vencimiento_consumo')
		);
	 $id=$this->input->post("id_consumo");
		if($this->consumo->actualizar($id,$datosConsumoEditado)){
			redirect('consumos/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
