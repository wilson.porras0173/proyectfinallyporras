<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("evento");
	}
	//renderiza la vista index de registros
	public function index()
	{
		$data["listadoEventos"]=
		$this->evento->obtenerTodos();
		$this->load->view('header');
		$this->load->view('eventos/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de registros
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('eventos/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarEvento(){
		$datosNuevoEvento=array(
      "descripcion_eve"=>$this->input->post('descripcion_eve'),
			"fecha_hora_eve"=>$this->input->post('fecha_hora_eve'),
			"lugar_eve"=>$this->input->post('lugar_eve'),
      "fk_id_te"=>$this->input->post('fk_id_te')
		);
    if($this->evento->insertar($datosNuevoEvento)){
				$this->session
				->set_flashdata('confirmacion',
			 'Evento insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('eventos/index');
	}
	//funcion para eliminar registros
	public function borrar($id_eve){
		if ($this->evento->eliminarPorId($id_eve)){
			$this->session
			->set_flashdata('confirmacion',
		 'Evento ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('eventos/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["eventoEditar"]=
			$this->evento->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("eventos/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosEventoEditado=array(
      "descripcion_eve"=>$this->input->post('descripcion_eve'),
			"fecha_hora_eve"=>$this->input->post('fecha_hora_eve'),
			"lugar_eve"=>$this->input->post('lugar_eve'),
      "fk_id_te"=>$this->input->post('fk_id_te')
		);
	 $id=$this->input->post("id_eve");
		if($this->evento->actualizar($id,$datosEventoEditado)){
			redirect('eventos/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
