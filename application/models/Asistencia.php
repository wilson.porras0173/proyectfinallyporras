<?php
   class Asistencia extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("asistencia",$datos);
     }
     //Funcion que consulta todos los registros de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_asi","asc");
        $result=$this->db->get("asistencia");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un registro se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_asi",$id);
        return $this->db->delete("asistencia");
     }
     //Consultando el registro por su id
     public function obtenerPorId($id){
        $this->db->where("id_asi",$id);
        $asistencia=$this->db->get("asistencia");
        if($asistencia->num_rows()>0){
          return $asistencia->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de registro
     public function actualizar($id,$datos){
       $this->db->where("id_asi",$id);
       return $this->db->update("asistencia",$datos);
     }

   }//Cierre de la clase (No borrar)














//
