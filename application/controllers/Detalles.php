<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalles extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("detalle");
	}
	//renderiza la vista index de registros
	public function index()
	{
		$data["listadoDetalles"]=
		$this->detalle->obtenerTodos();
		$this->load->view('header');
		$this->load->view('detalles/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de registros
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('detalles/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarDetalle(){
		$datosNuevoDetalle=array(
      "fk_id_lec"=>$this->input->post('fk_id_lec'),
      "fk_id_rec"=>$this->input->post('fk_id_rec'),
      "cantidad_det"=>$this->input->post('cantidad_det'),
			"detalle_det"=>$this->input->post('detalle_det'),
			"valor_unitario_det"=>$this->input->post('valor_unitario_det'),
      "subtotal_det"=>$this->input->post('subtotal_det'),
      "iva_det"=>$this->input->post('iva_det')
		);
    if($this->detalle->insertar($datosNuevoDetalle)){
				$this->session
				->set_flashdata('confirmacion',
			 'Detalle insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('detalles/index');
	}
	//funcion para eliminar registros
	public function borrar($id_det){
		if ($this->detalle->eliminarPorId($id_det)){
			$this->session
			->set_flashdata('confirmacion',
		 'Detalle ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('detalles/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["detalleEditar"]=
			$this->detalle->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("detalles/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosDetalleEditado=array(
      "fk_id_lec"=>$this->input->post('fk_id_lec'),
      "fk_id_rec"=>$this->input->post('fk_id_rec'),
      "cantidad_det"=>$this->input->post('cantidad_det'),
			"detalle_det"=>$this->input->post('detalle_det'),
			"valor_unitario_det"=>$this->input->post('valor_unitario_det'),
      "subtotal_det"=>$this->input->post('subtotal_det'),
      "iva_det"=>$this->input->post('iva_det')
		);
	 $id=$this->input->post("id_det");
		if($this->detalle->actualizar($id,$datosDetalleEditado)){
			redirect('detalles/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
