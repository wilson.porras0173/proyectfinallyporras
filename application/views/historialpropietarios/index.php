<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalHistorialpropietarios = 0;
$mostSeatsStadium = null;

if ($listadoHistorialpropietarios) {
    $totalHistorialpropietarios = sizeof($listadoHistorialpropietarios);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoHistorialpropietarios as $historialpropietarioTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE HISTORIAL PROPIETARIO</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('historialpropietarioS/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar historial propietario</a>
</center>
<br>
</div>
<br>
<?php if ($listadoHistorialpropietarios): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_historialpropietarios">
      <thead>
        <tr>
          <th style="color:white;">ID</th>
          <th style="color:white;">FK_ID_MED</th>
          <th style="color:white;">FK_ID_SOC</th>
          <th style="color:white;">ACTUALIZACION</th>
          <th style="color:white;">ESTADO</th>
          <th style="color:white;">OBSERVACION</th>
          <th style="color:white;">FECHA CAMBIO</th>
          <th style="color:white;">CREACION</th>
          <th style="color:white;">PROPIETARIO ACTUAL</th>
          <th style="color:white;">ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoHistorialpropietarios as $historialpropietarioTemporal): ?>
          <tr>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->id_his ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->fk_id_med ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->fk_id_soc ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->actualizacion_his ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->estado_his ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->observacion_his ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->fecha_cambio_his ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->creacion_his ?></td>
            <td style="color:white;"><?php echo $historialpropietarioTemporal->propietario_actual_his ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/historialpropietarios/actualizar/<?php echo $historialpropietarioTemporal->id_his; ?>" title="Editar evento">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/historialpropietarios/borrar/<?php echo $historialpropietarioTemporal->id_his; ?>" title="Eliminar evento" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalHistorialpropietarios; ?>
        </h5>
        <p class="card-text">Registro de evento</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_historialpropietarios").DataTable();
</script>
