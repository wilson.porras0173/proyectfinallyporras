<?php
   class Socio extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("socio",$datos);
     }
     //Funcion que consulta todos los registros de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_soc","asc");
        $result=$this->db->get("socio");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un registro se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_soc",$id);
        return $this->db->delete("socio");
     }
     //Consultando el registro por su id
     public function obtenerPorId($id){
        $this->db->where("id_soc",$id);
        $socio=$this->db->get("socio");
        if($socio->num_rows()>0){
          return $socio->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de registro
     public function actualizar($id,$datos){
       $this->db->where("id_soc",$id);
       return $this->db->update("socio",$datos);
     }

   }//Cierre de la clase (No borrar)














//
