<!DOCTYPE html>
<html lang="en">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>PROYECT FINALLY</title>

<!-- CSS de Bootstrap 5 -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- JS de Bootstrap 5 (si necesitas funcionalidades interactivas) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>


<!-- plugins:css -->
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/mdi/css/materialdesignicons.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/css/vendor.bundle.base.css') ?>">
<!-- endinject -->
<!-- Plugin css for this page -->
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/jvectormap/jquery-jvectormap.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/flag-icon-css/css/flag-icon.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/owl-carousel-2/owl.carousel.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/owl-carousel-2/owl.theme.default.min.css'); ?>">
<!-- End plugin css for this page -->
<!-- inject:css -->
<!-- endinject -->
<!-- Layout styles -->
<link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/style.css');?>">
<!-- End layout styles -->
<link rel="shortcut icon" href="<?php echo base_url('assets/image/logoUTC.png'); ?>" />
<!-- Importacion jquery -->
<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
<!-- Importacion de jquery validate -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
	 jQuery.validator.addMethod("letras", function(value, element) {
		 //return this.optional(element) || /^[a-z]+$/i.test(value);
		 return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);

	 }, "Este campo solo acepta letras");
 </script>
 <!-- importacion toastr js-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<!-- importacion toastr css-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!-- importacion de cdn datatables-->
<link rel="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" href="/css/master.css">
<!-- importacion de javascript-->
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<!-- importacion de cdn datatables-->
<link rel="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" href="/css/master.css">
<!-- importacion de javascript-->
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<!-- importacion de fileinput .js cdn-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.2/js/fileinput.min.js" integrity="sha512-XxRivO6jA7xU9a0ozATMIFQFdNySyRrB8uE1QctFmjTTGSGUj9tC7CpnVf7xq1e/QeVhbY9ZLbxEzPFIWpW+xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- importacion de fileinput .js cdn extencion css-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.2/css/fileinput.min.css" integrity="sha512-sHiVTDN234pgseKqjDwH39VjS9DkyffX4S00kuAWWq+FrTq7HlFjPoWbfX/QFAxkdG9i9/1ftdG2sS+XWLcJmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
          <a class="sidebar-brand brand-logo" href="index.html"><img src="plantilla/assets/images/logo.svg" alt="logo" /></a>
          <a class="sidebar-brand brand-logo-mini" href="index.html"><img src="plantilla/assets/images/logo-mini.svg" alt="logo" /></a>
        </div>
        <ul class="nav">
          <li class="nav-item profile">
            <div class="profile-desc">
              <div class="profile-pic">
                <div class="count-indicator">
                  <img class="img-xs rounded-circle " src="<?php echo base_url(); ?>/assets/image/logofacebook.jpg" alt="">
                  <span class="count bg-success"></span>
                </div>
                <div class="profile-name">
                  <h5 class="mb-0 font-weight-normal">Wilson Porras</h5>
                  <span>Activo</span>
                </div>
              </div>
              <a href="#" id="profile-dropdown" data-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a>
              <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list" aria-labelledby="profile-dropdown">
                <a href="#" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-settings text-primary"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject ellipsis mb-1 text-small">Account settings</p>
                  </div>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-onepassword  text-info"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject ellipsis mb-1 text-small">Change Password</p>
                  </div>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-calendar-today text-success"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject ellipsis mb-1 text-small">To-do list</p>
                  </div>
                </a>
              </div>
            </div>
          </li>
          <li class="nav-item nav-category">
            <span class="nav-link">Navigation</span>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" href="<?php echo base_url(); ?>/welcome_message">
              <span class="menu-icon">
                <i class="mdi mdi-speedometer"></i>
              </span>
              <span class="menu-title">INICIO</span>
            </a>
          </li>




					<!--INICIO PERFIL-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            PERFIL
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/perfiles/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/perfiles/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN PERFIL-->

					<!--INICIO USUARIO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            USUARIO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/usuarios/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/usuarios/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN USUARIO-->

					<!--INICIO COMUNICADO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            COMUNICADO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/comunicados/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/comunicados/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN COMUNICADO-->

					<!--INICIO TIPO EVENTO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            TIPO EVENTO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/tipoeventos/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/tipoeventos/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN TIPO EVENTO-->

					<!--INICIO EVENTO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            EVENTO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/eventos/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/eventos/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN EVENTO-->

					<!--INICIO RUTA-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            RUTA
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/rutas/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/rutas/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN RUTA-->

					<!--INICIO IMPUESTO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            IMPUESTO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/impuestos/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/impuestos/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN IMPUESTO-->

					<!--INICIO CONFIGURACION-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            CONFIGURACION
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/configuraciones/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/configuraciones/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN CONFIGURACION-->

					<!--INICIO TARIFA-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            TARIFA
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/tarifas/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/tarifas/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN TARIFA-->

					<!--INICIO EXCEDENTE-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            EXCEDENTE
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/excedentes/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/excedentes/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN EXCEDENTE-->

					<!--INICIO MEDIDOR-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            MEDIDOR
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/medidores/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/medidores/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN MEDIDOR-->

					<!--INICIO CONSUMO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            CONSUMO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/consumos/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/consumos/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN CONSUMO-->

					<!--INICIO SOCIOS-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            SOCIO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/socios/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/socios/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN SOCIOS-->

					<!--INICIO ASISTENCIAS-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            ASISTENCIA
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/asistencias/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/asistencias/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN ASISTENCIAS-->

					<!--INICIO HISTORIAL PROPIETARIO-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            HISTORIAL PROPIETARIO
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/historialpropietarios/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/historialpropietarios/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN HISTORIAL PROPIETARIO-->

					<!--INICIO RECAUDACION-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            RECAUDACION
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/recaudaciones/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/recaudaciones/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN RECAUDACION-->


					<!--INICIO LECTURA-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            LECTURA
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/lecturas/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/lecturas/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN LECTURA-->


					<!--INICIO DETALLE-->
									<li class="nav-item dropdown">
						          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						            DETALLE
						          </a>
						          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/detalles/nuevo">Nuevo</a></li>
						            <li><a class="dropdown-item" href="<?php echo site_url(); ?>/detalles/index">Listado</a></li>
						            <li><hr class="dropdown-divider"></li>
						          </ul>
						        </li>
					<!--FIN DETALLE-->

          <li class="">
            <a class="nav-link" href="">

            </a>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar p-0 fixed-top d-flex flex-row">
          <div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
            <a class="navbar-brand brand-logo-mini" href="index.html"><img src="plantilla/assets/images/logo-mini.svg" alt="logo" /></a>
          </div>
          <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
              <span class="mdi mdi-menu"></span>
            </button>

            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item dropdown d-none d-lg-block">
                <a class="nav-link btn btn-success create-new-button" id="createbuttonDropdown" data-toggle="dropdown" aria-expanded="false" href="#">+ Create New Project</a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="createbuttonDropdown">
                  <h6 class="p-3 mb-0">Projects</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-file-outline text-primary"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Software Development</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-web text-info"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">UI Development</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-layers text-danger"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Software Testing</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">See all projects</p>
                </div>
              </li>
              <li class="nav-item nav-settings d-none d-lg-block">
                <a class="nav-link" href="#">
                  <i class="mdi mdi-view-grid"></i>
                </a>
              </li>
              <li class="nav-item dropdown border-left">
                <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                  <i class="mdi mdi-email"></i>
                  <span class="count bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                  <h6 class="p-3 mb-0">Messages</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face4.jpg" alt="image" class="rounded-circle profile-pic">
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Mark send you a message</p>
                      <p class="text-muted mb-0"> 1 Minutes ago </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face2.jpg" alt="image" class="rounded-circle profile-pic">
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Cregh send you a message</p>
                      <p class="text-muted mb-0"> 15 Minutes ago </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face3.jpg" alt="image" class="rounded-circle profile-pic">
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Profile picture updated</p>
                      <p class="text-muted mb-0"> 18 Minutes ago </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">4 new messages</p>
                </div>
              </li>
              <li class="nav-item dropdown border-left">
                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-bell"></i>
                  <span class="count bg-danger"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                  <h6 class="p-3 mb-0">Notifications</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-calendar text-success"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Event today</p>
                      <p class="text-muted ellipsis mb-0"> Just a reminder that you have an event today </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-settings text-danger"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Settings</p>
                      <p class="text-muted ellipsis mb-0"> Update dashboard </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-link-variant text-warning"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Launch Admin</p>
                      <p class="text-muted ellipsis mb-0"> New admin wow! </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">See all notifications</p>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
                  <div class="navbar-profile">
                    <img class="img-xs rounded-circle" src="<?php echo base_url(); ?>assets/image/logofacebook.jpg" alt="">
                    <p class="mb-0 d-none d-sm-block navbar-profile-name">Wilson Porras</p>
                    <i class="mdi mdi-menu-down d-none d-sm-block"></i>
                  </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
                  <h6 class="p-3 mb-0">Profile</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-settings text-success"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Cerrar sesion</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>

                </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
              <span class="mdi mdi-format-line-spacing"></span>
            </button>
          </div>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
