<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVA LECTURA</h1>
<form class="" id="frm_nuevo_lectura" action="<?php echo site_url('lecturas/guardarLectura'); ?>" method="post" enctype="multipart/form-data">

<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="anio_lec">AÑO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el año" class="form-control" required name="anio_lec" id="anio_lec">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="mes_lec">MES:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el mes" class="form-control" required name="mes_lec" id="mes_lec">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="estado_lec">ESTADO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="estado_lec" id="estado_lec">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="lectura_anterior_lec">LECTURA ANTERIOR:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la lectura anterior" class="form-control" required name="lectura_anterior_lec" id="lectura_anterior_lec">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="lectura_actual_lec">LECTURA ACTUAL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la lectura actual" class="form-control" required name="lectura_actual_lec" id="lectura_actual_lec">
            </div>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_creacion_lec">FECHA CREACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la fecha de creacion" class="form-control" required name="fecha_creacion_lec" id="fecha_creacion_lec">
            </div>
        </div>
    </div>
</center>
    <br>

<center>
    <div class="row">
      <div class="col-md-1">
      </div>
      <div class="col-md-9">
        <div class="form-group">
            <label for="fecha_actualizacion_lec">FECHA ACTUALIZACION:
                <span class="obligatorio">(Required)</span>
            </label>
            <input type="text" placeholder="Ingrese la fecha de actualizacion" class="form-control" required name="fecha_actualizacion_lec" id="fecha_actualizacion_lec">
        </div>
      </div>
    </div>
</center>
<br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="fk_id_his">FK_ID_HIS:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el id de historial de propietarios" class="form-control" required name="fk_id_his" id="fk_id_his">
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="fk_id_consumo">FK_ID_CONSUMO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el id de consumo" class="form-control" required name="fk_id_consumo" id="fk_id_consumo">
                </div>
            </div>
        </div>
    </center>
        <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/lecturas/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_lectura").validate({
    rules:{
        anio_lec:{
            required:true,
        },
        mes_lec:{
            required:true,
        },
        estado_lec:{
          required:true,
        },
        lectura_anterior_lec:{
          required:true,
        },
        lectura_actual_lec:{
          required:true,
        },
        fecha_creacion_lec:{
          required:true,
        },
        fecha_actualizacion_lec:{
          required:true,
        },
        fk_id_his:{
          required:true,
        },
        fk_id_consumo:{
          required:true,
        }
    },
    messages:{
        anio_lec:{
            required:"Porfavor, ingrese el año",
        },
        mes_lec:{
            required:"Porfavor, ingrese el mes",
        },
        estado_lec:{
          required:"Porfavor, ingrese el estado",
        },
        lectura_anterior_lec:{
          required:"Porfavor, ingrese la lectura anterior",
        },
        lectura_actual_lec:{
          required:"Porfavor, ingrese la lectura actual",
        },
        fecha_creacion_lec:{
          required:"Porfavor, ingrese la fecha de creacion",
        },
        fecha_actualizacion_lec:{
          required:"Porfavor, ingrese la fecha de actualizacion",
        },
        fk_id_his:{
          required:"Porfavor, ingreso el id de historial de propietario",
        },
        fk_id_consumo:{
          required:"Porfavor, ingreso el id de consumo",
        }
      }
});
</script>
