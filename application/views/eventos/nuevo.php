<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO EVENTO</h1>
<form class="" id="frm_nuevo_evento" action="<?php echo site_url('eventos/guardarEvento'); ?>" method="post" enctype="multipart/form-data">

<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="descripcion_eve">DESCRIPCION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese una descripcion" class="form-control" required name="descripcion_eve" id="descripcion_eve">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_hora_eve">FECHA_HORA:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="EJEMPLO: 2001-10-10 20:20:20" class="form-control" required name="fecha_hora_eve" id="fecha_hora_eve">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="lugar_eve">LUGAR:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el lugar del evento" class="form-control" required name="lugar_eve" id="lugar_eve">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_te">FK_ID_TE:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el id de tipo de  evento" class="form-control" required name="fk_id_te" id="fk_id_te">
            </div>
        </div>
    </div>
</center>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/registros/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_evento").validate({
    rules:{
        descripcion_eve:{
            required:true,
        },
        fecha_hora_eve:{
            required:true,
        },
        lugar_eve:{
          required:true,
        },
        fk_id_te:{
          required:true,
        }
    },
    messages:{
        descripcion_eve:{
            required:"Porfavor, este campo solo admite letras",
        },
        fecha_hora_eve:{
            required:"Porfavor, este campo solo admite numeros",
        },
        lugar_eve:{
          required:"Porfavor, este campo solo admite letras",
        },
        fk_id_te:{
          required:"Porfavor, ingreso el id de tipo de evento",
        }
      }
});
</script>
