<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencias extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("asistencia");
	}
	//renderiza la vista index de registros
	public function index()
	{
		$data["listadoAsistencias"]=
		$this->asistencia->obtenerTodos();
		$this->load->view('header');
		$this->load->view('asistencias/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de registros
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('asistencias/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarAsistencia(){
		$datosNuevoAsistencia=array(
      "fk_id_eve"=>$this->input->post('fk_id_eve'),
			"fk_id_soc"=>$this->input->post('fk_id_soc'),
			"tipo_asi"=>$this->input->post('tipo_asi'),
      "valor_asi"=>$this->input->post('valor_asi'),
      "atraso_asi"=>$this->input->post('atraso_asi'),
      "valor_atraso_asi"=>$this->input->post('valor_atraso_asi'),
      "creacion_asi"=>$this->input->post('creacion_asi'),
      "actualizacion_asi"=>$this->input->post('actualizacion_asi')
		);
    if($this->asistencia->insertar($datosNuevoAsistencia)){
				$this->session
				->set_flashdata('confirmacion',
			 'Asistencia insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('asistencias/index');
	}
	//funcion para eliminar registros
	public function borrar($id_asi){
		if ($this->asistencia->eliminarPorId($id_asi)){
			$this->session
			->set_flashdata('confirmacion',
		 'Asistencia ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('asistencias/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["asistenciaEditar"]=
			$this->asistencia->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("asistencias/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosAsistenciaEditado=array(
      "fk_id_eve"=>$this->input->post('fk_id_eve'),
			"fk_id_soc"=>$this->input->post('fk_id_soc'),
			"tipo_asi"=>$this->input->post('tipo_asi'),
      "valor_asi"=>$this->input->post('valor_asi'),
      "atraso_asi"=>$this->input->post('atraso_asi'),
      "valor_atraso_asi"=>$this->input->post('valor_atraso_asi'),
      "creacion_asi"=>$this->input->post('creacion_asi'),
      "actualizacion_asi"=>$this->input->post('actualizacion_asi')
		);
	 $id=$this->input->post("id_asi");
		if($this->asistencia->actualizar($id,$datosAsistenciaEditado)){
			redirect('asistencias/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
