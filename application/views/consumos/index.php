<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalConsumos = 0;
$mostSeatsStadium = null;

if ($listadoConsumos) {
    $totalConsumos = sizeof($listadoConsumos);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoConsumos as $consumoTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO CONSUMO</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('consumos/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Consumo</a>
</center>
<br>
</div>
<br>
<?php if ($listadoConsumos): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_consumos">
      <thead>
        <tr>
          <th style="color:white;">ID</th>
          <th style="color:white;">AÑO</th>
          <th style="color:white;">MES</th>
          <th style="color:white;">ESTADO</th>
          <th style="color:white;">FECHA CREACION</th>
          <th style="color:white;">FECHA ACTUALIZACION</th>
          <th style="color:white;">NUMERO MES</th>
          <th style="color:white;">FECHA VENCIMIENTO</th>
          <th style="color:white;">ACCIONES</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoConsumos as $consumoTemporal): ?>
          <tr>
            <td style="color:white;"><?php echo $consumoTemporal->id_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->anio_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->mes_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->estado_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->fecha_creacion_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->fecha_actualizacion_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->numero_mes_consumo ?></td>
            <td style="color:white;"><?php echo $consumoTemporal->fecha_vencimiento_consumo ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/consumos/actualizar/<?php echo $consumoTemporal->id_consumo; ?>" title="Editar consumo">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/consumos/borrar/<?php echo $consumoTemporal->id_consumo; ?>" title="Eliminar consumo" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalConsumos; ?>
        </h5>
        <p class="card-text">Registro Consumo</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_consumos").DataTable();
</script>
