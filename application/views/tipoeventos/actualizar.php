<h1 class="text-center"><i class=""></i> ACTUALIZAR DATOS</h1>
<form class=""
id="frm_actualizar_tipoevento"
action="<?php echo site_url('tipoeventos/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
          <div class="col-md-4">
            <input type="hidden" name="id_te" id="id_te" value="<?php echo $tipoeventoEditar->id_te; ?>">
              <label for="">NOMBRE:
                <span class="obligatorio">(Required)</span>
              </label>
              <br>
              <input type="text"
              placeholder="ingrese el nombre"
              class="form-control"
              required
              name="nombre_te" value="<?php echo $tipoeventoEditar->nombre_te; ?>"
              id="nombre_te">
              </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="estado_te">ESTADO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="estado_te"  value="<?php echo $tipoeventoEditar->estado_te; ?>" id="estado_te">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="creacion_te">CREACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="EJEMPLO: 2001-10-10 20:20:20" class="form-control" required name="creacion_te" value="<?php echo $tipoeventoEditar->creacion_te; ?>" id="creacion_te">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="actualizacion_te">ACTUALIZAR:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="EJEMPLO: 2001-10-10 20:20:20" class="form-control" required name="actualizacion_te" value="<?php echo $tipoeventoEditar->actualizacion_te; ?>" id="actualizacion_te">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-success">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/tipoeventos/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_tipoevento").validate({
   rules:{
     nombre_te:{
       required:true
     },
     estado_te:{
       required:true
     },
     creacion_te:{
       required:true
     },
     actualizacion_te:{
       required:true
     }
   },
   messages:{
     nombre_te:{
         required:"Porfavor, este campo solo admite letras",
     },
     estado_te:{
         required:"Porfavor, este campo solo admite letras",
     },
     creacion_te:{
       required:"Porfavor, este campo solo admite numeros",
     },
     actualizacion_te:{
       required:"Porfavor, este campo solo admite numeros",
     }
   }
 });

 </script>
