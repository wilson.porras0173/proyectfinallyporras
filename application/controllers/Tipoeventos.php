<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipoeventos extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("tipoevento");
	}
	//renderiza la vista index de tipoevento
	public function index()
	{
		$data["listadoTipoeventos"]=
		$this->tipoevento->obtenerTodos();
		$this->load->view('header');
		$this->load->view('tipoeventos/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de tipoevento
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('tipoeventos/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarTipoevento(){
		$datosNuevoTipoevento=array(
			"nombre_te"=>$this->input->post('nombre_te'),
      "estado_te"=>$this->input->post('estado_te'),
			"creacion_te"=>$this->input->post('creacion_te'),
      "actualizacion_te"=>$this->input->post('actualizacion_te')
		);
    if($this->tipoevento->insertar($datosNuevoTipoevento)){
				$this->session
				->set_flashdata('confirmacion',
			 'Evento insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('tipoeventos/index');
	}
	//funcion para eliminar tipoevento
	public function borrar($id_te){
		if ($this->tipoevento->eliminarPorId($id_te)){
			$this->session
			->set_flashdata('confirmacion',
		 'Evento ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('tipoeventos/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["tipoeventoEditar"]=
			$this->tipoevento->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("tipoeventos/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosTipoeventoEditado=array(
      "nombre_te"=>$this->input->post('nombre_te'),
      "estado_te"=>$this->input->post('estado_te'),
			"creacion_te"=>$this->input->post('creacion_te'),
      "actualizacion_te"=>$this->input->post('actualizacion_te')
		);
	 $id=$this->input->post("id_te");
		if($this->tipoevento->actualizar($id,$datosTipoeventoEditado)){
			redirect('tipoeventos/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
