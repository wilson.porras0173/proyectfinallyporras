<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO MEDIDOR</h1>
<form class="" id="frm_nuevo_medidor" action="<?php echo site_url('medidores/guardarMedidor'); ?>" method="post" enctype="multipart/form-data">

<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_rut">ID_RUT:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el id de ruta" class="form-control" required name="fk_id_rut" id="fk_id_rut">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_tar">ID_TAR:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el id de tarifa" class="form-control" required name="fk_id_tar" id="fk_id_tar">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="numero_med">NUMERO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el numero de medidor" class="form-control" required name="numero_med" id="numero_med">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="serie_med">SERIE:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la serie del medidor" class="form-control" required name="serie_med" id="serie_med">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label for="marca_med">MARCA:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la marca del medidor" class="form-control" required name="marca_med" id="marca_med">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="observacion_med">OBSERVACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese una observacion" class="form-control" required name="observacion_med" id="observacion_med">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="estado_med">ESTADO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el estado del medidor" class="form-control" required name="estado_med" id="estado_med">
                </div>
            </div>
        </div>
    </center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="foto_med">FOTO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese una foto" class="form-control" required name="foto_med" id="foto_med">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="creacion_med">CREACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la creacion" class="form-control" required name="creacion_med" id="creacion_med">
                </div>
            </div>
        </div>
    </center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="actualizacion_med">ACTUALIZACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la actualizacion" class="form-control" required name="actualizacion_med" id="actualizacion_med">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="lectura_inicial_med">LECTURA INICIAL:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la lectura inicial" class="form-control" required name="lectura_inicial_med" id="lectura_inicial_med">
                </div>
            </div>
        </div>
    </center>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/medidores/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_medidor").validate({
    rules:{
        fk_id_rut:{
            required:true,
        },
        fk_id_tar:{
          required:true,
        },
        numero_med:{
          required:true,
        },
        serie_med:{
          required:true,
        },
        marca_med:{
          required:true,
        },
        observacion_med:{
          required:true,
        },
        estado_med:{
          required:true,
        },
        foto_med:{
          required:true,
        },
        creacion_med:{
          required:true,
        },
        actualizacion_med:{
          required:true,
        },

        lectura_inicial_med:{
          required:true,
        }
    },
    messages:{
        fk_id_rut:{
            required:"Porfavor, ingrese el id de ruta",
        },
        fk_id_tar:{
            required:"Porfavor, ingrese el id de tarifa",
        },
        numero_med:{
          required:"Porfavor, ingrese el numero de medidor",
        },
        serie_med:{
          required:"Porfavor, ingrese la serie del medidor",
        },
        marca_med:{
          required:"Porfavor, ingrese la marca del medidor",
        },
        observacion_med:{
          required:"Porfavor, ingrese la observacion",
        },
        estado_med:{
          required:"Porfavor, ingrese el estado",
        },
        foto_med:{
          required:"Porfavor, ingrese la foto",
        },
        creacion_med:{
          required:"Porfavor, ingrese la creacion",
        },
        actualizacion_med:{
          required:"Porfavor, ingrese la actualizacion",
        },
        lectura_inicial_med:{
          required:"Porfavor, ingrese la lectura inicial",
        }
      }
});
</script>
